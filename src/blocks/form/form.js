$('.form__input').change(function() {
  $(this).addClass('form__input--active');
  if( !$(this).val() ) {
    $(this).removeClass('form__input--active');
  }
});

$('.form__input').change();

if ($(".form__input--calendar")[0]){
  $(function() {
    $('.form__input--calendar').datepicker({
      yearRange: "-100:+3",
      defaultDate: null,
      changeMonth: true,
      changeYear: true,
      dateFormat: "dd.mm.yy",
      numberOfMonths: 1,
      firstDay: 1
    });
  });
}


window.addEventListener('load', function() {
  // Fetch all the forms we want to apply custom Bootstrap validation styles to
  var forms = document.getElementsByClassName('form');
  // Loop over them and prevent submission
  var validation = Array.prototype.filter.call(forms, function(form) {
    form.addEventListener('submit', function(event) {
      if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
      }
      form.classList.add('was-validated');
//      $('.form').addClass('form__item--invalid');
      $('.form').find('.form-control:invalid').closest('.form__item').addClass('form__item--invalid');
    }, false);
  });
}, false);

$('.form').on('submit', function (e) {
    window.setTimeout(function () {
        var errors = $('.form__item--invalid')
        if (errors.length) {
            $('html, body').animate({ scrollTop: errors.offset().top - 100 }, 500);
        }
    }, 0);
});