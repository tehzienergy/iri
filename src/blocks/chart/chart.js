if ($('#chart').length > 0) {
  var ctx = document.getElementById("chart").getContext('2d');
  var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["Обмен информацией и коммуникации", "Обмен информацией и коммуникации", "Обмен информацией и коммуникации", 'Обмен информацией и коммуникации'],
      datasets: [{
        data: [35, 35, 15, 15],
        backgroundColor: [
                  '#753BBD',
                  '#61C377',
                  'red',
                  '#41C8F5'
              ],
        borderColor: [
                  '#753BBD',
                  '#61C377',
                  'red',
                  '#41C8F5'
              ],
        borderWidth: 1
          }]
    },
    options: {
      legend: {
        position: 'right',
        labels: {
            generateLabels: function(chart) {
                var data = chart.data;
                if (data.labels.length && data.datasets.length) {
                    return data.labels.map(function(label, i) {
                        var meta = chart.getDatasetMeta(0);
                        var ds = data.datasets[0];
                        var arc = meta.data[i];
                        var custom = arc && arc.custom || {};
                        var getValueAtIndexOrDefault = Chart.helpers.getValueAtIndexOrDefault;
                        var arcOpts = chart.options.elements.arc;
                        var fill = custom.backgroundColor ? custom.backgroundColor : getValueAtIndexOrDefault(ds.backgroundColor, i, arcOpts.backgroundColor);
                        var stroke = custom.borderColor ? custom.borderColor : getValueAtIndexOrDefault(ds.borderColor, i, arcOpts.borderColor);
                        var bw = custom.borderWidth ? custom.borderWidth : getValueAtIndexOrDefault(ds.borderWidth, i, arcOpts.borderWidth);

          // We get the value of the current label
          var value = chart.config.data.datasets[arc._datasetIndex].data[arc._index];

                        return {
                            // Instead of `text: label,`
                            // We add the value to the string
                            text: label + ': ' + value,
                            fillStyle: fill,
                            strokeStyle: stroke,
                            lineWidth: bw,
                            hidden: isNaN(ds.data[i]) || meta.data[i].hidden,
                            index: i
                        };
                    });
                } else {
                    return [];
                }
            },
          usePointStyle: true,
          fontSize: 12,
          fontColor: '#000',
          padding: 20,
          fontFamily: "'HelveticaNeue', 'Arial', sans-serif"
        }
      },
      responsive: false,
      maintainAspectRatio: true,
    }
  });
}