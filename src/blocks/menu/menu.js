$('.menu__btn').click(function(e) {
  e.preventDefault();
  $(this).toggleClass('menu__btn--opened');
  $('.menu__content').toggleClass('menu__content--opened');
  $('body').toggleClass('body--fixed');
});