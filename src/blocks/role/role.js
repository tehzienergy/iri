$('.role__close').click(function(e){
  e.preventDefault();
  $('.role').fadeOut('fast');
});



$(".js-role-select").change(function() {
  var roleSelectedIndexList = [];
  var rolesSelectedList = [];
  $(this).find('option:selected').each(function() {
    var role = $(this).data('role');
    var roleText = $(this).text();
    var roleColumnIndex = $('.actions__row--header').find('#' + role).index();
    roleSelectedIndexList.push(roleColumnIndex);
    rolesSelectedList.push(roleText);
  })
  $('.actions__cell').addClass('actions__cell--inactive');
  $('.actions__cell--header').removeClass('actions__cell--inactive');
  
  $.each(roleSelectedIndexList, function(index, val) {
    $('.actions__row').each(function() {
      $(this).find('.actions__cell').eq(val).removeClass('actions__cell--inactive');
    })
  });
  
  $('.role__selected-list').empty();
  var rolesList = rolesSelectedList.join(", ");
  $('.role__selected-list').text(rolesList);
  
  if($('.select2-container--default .select2-selection--multiple .select2-selection__rendered').length) {
    const ps = new PerfectScrollbar('.select2-container--default .select2-selection--multiple .select2-selection__rendered');
  }
});