$('.select__input').each(function() {
  var select = $(this);
  select.select2({
    minimumResultsForSearch: -1,
    containerCssClass: 'select--form',
    placeholder: '',
    width: '100%',
    dropdownAutoWidth: true,
    dropdownCssClass: 'select__dropdown--common',
    dropdownParent: select.closest('.select')
  });
});

$('.select__input--multiple').each(function() {
  var select = $(this);
  select.select2({
    minimumResultsForSearch: -1,
    containerCssClass: 'select--form',
    placeholder: '',
    width: '100%',
    closeOnSelect: false,
    dropdownAutoWidth: true,
    dropdownCssClass: 'select__dropdown--common',
    dropdownParent: select.closest('.select')
  });
});

$('.select__input--simple').each(function() {
  var select = $(this);
  select.select2({
    minimumResultsForSearch: -1,
    containerCssClass: 'select--simple',
    placeholder: '',
    width: '100%',
    dropdownAutoWidth: true,
    dropdownCssClass: 'select__dropdown--common',
    dropdownParent: select.closest('.select')
  });
});

$('.select__input--list').each(function() {
  var select = $(this);
  select.select2({
    minimumResultsForSearch: -1,
    containerCssClass: 'select--list',
    placeholder: '',
    width: '100%',
    dropdownAutoWidth: true,
    dropdownCssClass: 'select__dropdown--common select__dropdown--list',
    dropdownParent: select.closest('.select')
  });
});

$('.select__input').on('select2:select', function (e) {
  // Do something
  $(this).closest('.form__item').find('.form__label').addClass('form__label--active');
});